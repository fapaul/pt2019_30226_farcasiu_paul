package monom;



public class Monom implements Comparable<Monom>{

	private int coef,exp;
	private float coefInt;
	private String monom;

	public Monom(String monom)
	{
		this.monom=monom;
	}
	public Monom()
	{

	}

	public int getCoef() {
		return coef;
	}
	public void setCoef(int coef) {
		this.coef = coef;
	}
	public void setCoefInt(float coefInt) {
		this.coefInt = coefInt;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public String getMonom() {
		return monom;
	}
	public void setMonom(String monom) {
		this.monom = monom;
	}

	public int compareTo(Monom o)
	{	if (exp<o.exp)
		return 1;
	if (exp>o.exp)
		return -1;
	return 0;
	}

	public Monom aduna(Monom o)
	{
		if (this.getExp()==o.getExp())
			this.setCoef(this.getCoef()+o.getCoef());
		return this;
	}

	public Monom inmulteste(Monom a,Monom o)
	{	Monom t=new Monom("+0x^0");
	t.setCoef(a.getCoef()*o.getCoef());
	t.setExp(a.getExp()+o.getExp());
	return t;
	}



	public String toString()
	{	String s=new String();
	if (coef<0) 
		s=coef+"x^"+exp;
	else 
		s="+"+coef+"x^"+exp;
	return s;
	}
	public String toStringInt()
	{	String t=new String();
	if (coefInt<0) 
		t=coefInt+"x^"+exp;
	else 
		t="+"+coefInt+"x^"+exp;
	return t;
	}

}
