package polinom;
import java.util.*;

import monom.*;


public class Polinom {
	private String polinom;
	private ArrayList <Monom> monoame=new ArrayList<Monom>();
	private int gradMin,gradMax;

	public Polinom(String polinom)
	{
		this.polinom=polinom;

	}
	public Polinom()
	{
	}

	public String getPolinom() {
		return polinom;
	}


	public void setPolinom(String polinom) {
		this.polinom = polinom;
	}

	public void inMonoame() 
	{  
		String parts[]=polinom.split("(?=\\+|-)");
		for (String i:parts)
		{
			Monom a=new Monom(i);
			monoame.add(a);
		}
	}
	public void iaCoeficientiSiExponent () 
	{	
		for (Monom i:monoame)
		{
			String coef[]=i.getMonom().split("[x]");
			if (coef[0]=="-" || coef[0]=="+")
				i.setCoef(1);
			else 
				i.setCoef(Integer.parseInt(coef[0]));
			String exp[]=i.getMonom().split("(\\^)");
			i.setExp(Integer.parseInt(exp[1]));

		}	
	}

	public void sortare()
	{
		Collections.sort(monoame);
	}
	public String toString()
	{	String s=new String();
	for (Monom i:monoame)
		s+=i.toString();
	return s;
	}

	public int getGradMin()
	{
		for (Monom a:monoame)
		{	
			gradMin=a.getExp();
		}
		return gradMin;
	}

	public int getGradMax()
	{
		for (Monom a:monoame)
		{	if (a.getExp()>gradMax)
			gradMax=a.getExp();
		}
		return gradMax;
	}

	public ArrayList<Monom> retPoli()
	{	
		return monoame;
	}

}
