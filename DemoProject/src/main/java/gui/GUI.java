package gui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import polinom.*;
import monom.*;
import operatii.*;

public class GUI  {


	public GUI()
	{
	}

	{	

		JFrame frame=new JFrame("Polinom");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 200);


		JPanel panel=new JPanel();
		JButton add = new JButton("Adunare");
		JButton sub = new JButton("Scadere");
		JButton mul = new JButton("Inmultire");
		JButton div = new JButton("Impartire");
		JButton dif = new JButton("Derivare");
		JButton integ = new JButton("Integrare");

		panel.add(add);
		panel.add(sub);
		panel.add(mul);
		panel.add(div);
		panel.add(dif);
		panel.add(integ);
		frame.add(panel);
		frame.setContentPane(panel);
		frame.setVisible(true);


		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)  {
				JFrame frame2= new JFrame("Adunare");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JTextField tf2 = new JTextField("Introduceti al doilea polinom");
				final JLabel label = new JLabel("Polinomul dupa adunare");
				JButton b = new JButton("Aduna!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(tf2);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener (new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Polinom p1=new Polinom(tf1.getText());
						Polinom p2=new Polinom(tf2.getText());
						Operatii o=new Operatii();
						o.adunare(p1, p2);
						label.setText(o.toString());

					}
				});

			}
		});

		sub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2= new JFrame("Scadere");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JTextField tf2 = new JTextField("Introduceti al doilea polinom");
				final JLabel label = new JLabel("Polinomul dupa scadere");
				JButton b = new JButton("Scade!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(tf2);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Polinom p1=new Polinom(tf1.getText());
						Polinom p2=new Polinom(tf2.getText());
						Operatii o=new Operatii();
						o.scadere(p1, p2);
						label.setText(o.toString());
					}
				});

			}
		});

		mul.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2= new JFrame("Inmultire");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JTextField tf2 = new JTextField("Introduceti al doilea polinom");
				final JLabel label = new JLabel("Polinomul dupa inmultire");
				JButton b = new JButton("Inmulteste!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(tf2);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Polinom p1=new Polinom(tf1.getText());
						Polinom p2=new Polinom(tf2.getText());
						Operatii o=new Operatii();
						o.inmultire(p1, p2);
						label.setText(o.toString());
					}
				});

			}
		});


		div.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2= new JFrame("Impartire");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JTextField tf2 = new JTextField("Introduceti al doilea polinom");
				final JLabel label = new JLabel("Polinomul dupa impartire");
				JButton b = new JButton("Imparte!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(tf2);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

					}
				});

			}
		});


		dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2= new JFrame("Derivare");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JLabel label = new JLabel("Polinomul dupa derivare");
				JButton b = new JButton("Deriveaza!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Polinom p=new Polinom(tf1.getText());
						Operatii o=new Operatii();
						o.derivare(p);
						label.setText(o.toString());
					}
				});

			}
		});


		integ.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame2= new JFrame("Integrare");
				frame2.setVisible(true);
				frame2.setSize(300,200);
				final JTextField tf1 = new JTextField("Introduceti primul polinom");
				final JLabel label = new JLabel("Polinomul dupa integrare");
				JButton b = new JButton("Integreaza!");
				JPanel panel=new JPanel();
				panel.add(tf1);
				panel.add(label);
				frame2.add(panel);
				panel.add(b,BorderLayout.PAGE_START);

				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Polinom p=new Polinom(tf1.getText());
						Operatii o=new Operatii();
						o.integrare(p);
						label.setText(o.toStringInt());
					}
				});

			}
		});

	}

}
