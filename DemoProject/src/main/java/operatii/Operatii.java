package operatii;
import polinom.*;
import monom.*;
import java.util.*;


public class Operatii {
	private ArrayList<Monom> listaMonoame=new ArrayList<Monom>();
	public Operatii()
	{
	}


	public void adunare(Polinom polinom1,Polinom polinom2)
	{	Polinom pam=new Polinom(polinom1.getPolinom()+polinom2.getPolinom());
	pam.inMonoame();
	pam.iaCoeficientiSiExponent();
	ArrayList<Monom> adunate=new ArrayList<Monom>();
	adunate.addAll(pam.retPoli());
	ArrayList<Monom> sch=new ArrayList<Monom>();
	sch.addAll(pam.retPoli());
	Collections.sort(adunate);
	Collections.sort(sch);
	for (int i=0;i<adunate.size();i++)
	{for (int j=i+1;j<adunate.size();j++)
		if (adunate.get(i).getExp()==(adunate.get(j).getExp()))
		{Monom monomInter=adunate.get(i);
		Monom monomInter2=adunate.get(j);
		Monom temporar=monomInter.aduna(monomInter2);
		sch.remove(monomInter);
		sch.remove(monomInter2);
		sch.add(temporar);
		}
	}
	Collections.sort(sch);
	listaMonoame.addAll(sch);
	}

	public void scadere(Polinom polinom1,Polinom polinom2)
	{	polinom2.inMonoame();
	String s=new String();
	polinom2.iaCoeficientiSiExponent();
	for (Monom i:polinom2.retPoli())
	{i.setCoef(-(i.getCoef()));
	s+=i.toString();}
	Polinom pam=new Polinom(polinom1.getPolinom()+s);
	pam.inMonoame();
	pam.iaCoeficientiSiExponent();
	ArrayList<Monom> adunate=new ArrayList<Monom>();
	adunate.addAll(pam.retPoli());
	ArrayList<Monom> sch=new ArrayList<Monom>();
	sch.addAll(pam.retPoli());
	Collections.sort(adunate);
	Collections.sort(sch);
	for (int i=0;i<adunate.size();i++)
	{for (int j=i+1;j<adunate.size();j++)
		if (adunate.get(i).getExp()==(adunate.get(j).getExp()))
		{Monom monomInter=adunate.get(i);
		Monom monomInter2=adunate.get(j);
		Monom temporar=monomInter.aduna(monomInter2);
		sch.remove(monomInter);
		sch.remove(monomInter2);
		sch.add(temporar);
		}
	}
	Collections.sort(sch);
	listaMonoame.addAll(sch);
	}

	public void derivare(Polinom p)
	{
		p.inMonoame();
		p.iaCoeficientiSiExponent();
		p.sortare();
		for (Monom i:p.retPoli())
		{
			if (i.getExp()!=0)
			{i.setCoef(i.getCoef()*i.getExp());
			i.setExp(i.getExp()-1);
			listaMonoame.add(i);
			}
			if (i.getExp()==0)
			{Monom m=new Monom("0");
			listaMonoame.add(m);
			}
		}
	}

	public void integrare(Polinom p)
	{	float c;
	p.inMonoame();
	p.iaCoeficientiSiExponent();
	p.sortare();
	for (Monom i:p.retPoli())
	{
		{c=((float)i.getCoef())/((float)(i.getExp()+1));
		i.setCoefInt(c);
		i.setExp(i.getExp()+1);
		listaMonoame.add(i);
		}
	}
	}

	public void inmultire(Polinom polinom1,Polinom polinom2)
	{
		polinom1.inMonoame();
		polinom2.inMonoame();
		polinom1.iaCoeficientiSiExponent();
		polinom2.iaCoeficientiSiExponent();
		ArrayList<Monom> inm1=new ArrayList<Monom>();
		ArrayList<Monom> inm2=new ArrayList<Monom>();
		ArrayList<Monom> finali=new ArrayList<Monom>();
		inm1.addAll(polinom1.retPoli());
		inm2.addAll(polinom2.retPoli());
		for (int i=0;i<inm1.size();i++)
		{
			for (int j=0;j<inm2.size();j++) {
				Monom t=new Monom();
				t=t.inmulteste(inm1.get(i),inm2.get(j));
				finali.add(t);
			}
		}
		Collections.sort(finali);
		finali=constr(finali);
		listaMonoame.addAll(finali);
	}

	private ArrayList<Monom> constr(ArrayList<Monom> t)
	{	
		for (int i=0;i<t.size();i++)
		{for (int j=i+1;j<t.size();j++)
		{if (t.get(i).getExp()==t.get(j).getExp())
		{t.get(i).setCoef(t.get(i).getCoef()+t.get(j).getCoef());
		t.remove(t.get(j));
		}
		}
		}
		return t;
	}

	public String toString()
	{String s=new String();
	for (Monom i:listaMonoame)
	{
		s+=i.toString();
	}
	return s;
	}

	public String toStringInt()
	{String s=new String();
	for (Monom i:listaMonoame)
	{
		s+=i.toStringInt();
	}
	return s;
	}
}
